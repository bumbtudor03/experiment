![Status]

## Project :tada:

The project is developed using Node.js. To get ready, install packages using NPM:
```shell
npm install 
```

### JavaScript calls

A simple JavaScript call to launch the service can be written as:
```javascript
function launch() {
    // handle the server launch.
}

app.start(1234, launch);
```

### Node.js requirements

A Node.js app requires three things:
-[] NPM for package management
-[] JavaScript for programming interface
-[x] Passion, and sometimes patience. :wink: